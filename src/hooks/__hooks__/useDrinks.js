import { useState, useEffect } from "react";
import beerService from "../../services/beer.service";

export default function useDrinks(personId) {
  const [drinks, setDrinks] = useState([]);
  const [loading, setLoading] = useState(true);

  async function handleDrink() {
    const beer = beerService.createBeer();
    setDrinks(currentDrinks => [...currentDrinks, { ...beer, loading: true }]);
    await beerService.drinkBeer(personId, beer);
    setDrinks(currentDrinks =>
      currentDrinks.map(cd => {
        if (cd.id === beer.id) {
          return {
            ...beer,
            loading: false
          };
        }
        return cd;
      })
    );
  }

  async function reload(personId) {
    setLoading(true);
    const drinks = await beerService.getBeerCount(personId);
    setDrinks(drinks);
    setLoading(false);
  }

  function handleBeerAdded(beer) {
    setDrinks(currentDrinks => {
      const present = currentDrinks.find(drink => drink.id === beer.id);
      if (present) {
        return currentDrinks;
      }
      return [...currentDrinks, beer];
    });
  }

  useEffect(() => {
    reload(personId);
    const cancel = beerService.listenToBeers(personId, handleBeerAdded);
    return () => {
      cancel();
    };
  }, [personId]);

  return [drinks, loading, handleDrink];
}
