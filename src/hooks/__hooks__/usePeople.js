import { useState } from "react";

export default function usePeople(initial) {
  const [people, setPeople] = useState(initial);
  const [activePerson, setActivePerson] = useState(people[0]);

  function addPerson(person) {
    setPeople(prev => [...prev, person]);
  }

  function selectPerson(person) {
    setActivePerson(person);
  }

  return [activePerson, people, addPerson, selectPerson];
}
