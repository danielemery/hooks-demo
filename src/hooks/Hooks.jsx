import React, { useState } from "react";
import uuidv4 from "uuid/v4";

import usePeople from "./__hooks__/usePeople";
import constants from "../common/constants";
import PeopleMenu from "../common/PeopleMenu";
import HooksBeerCounter from "./HooksBeerCounter";
import AddPersonModal from "../common/AddPersonModal";

export default function Hooks() {
  const [activePerson, people, addPerson, selectPerson] = usePeople(
    constants.samplePeople
  );
  const [adding, setAdding] = useState(false);
  const [addName, setAddName] = useState("");

  function handleAddPerson() {
    addPerson({
      name: addName,
      id: uuidv4()
    });
    setAdding(false);
  }

  return (
    <div>
      <PeopleMenu
        people={people}
        activePerson={activePerson}
        handlePersonChange={selectPerson}
        handleAdd={() => setAdding(true)}
      />
      <HooksBeerCounter person={activePerson} />
      <AddPersonModal
        open={adding}
        value={addName}
        onChange={name => setAddName(name)}
        onClose={() => setAdding(false)}
        onSubmit={handleAddPerson}
      />
    </div>
  );
}
