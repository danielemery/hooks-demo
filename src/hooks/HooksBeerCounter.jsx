import React from "react";

import BeerCounter from "../common/BeerCounter";
import useDrinks from "./__hooks__/useDrinks";

export default function(props) {
  const { person } = props;
  const { id: personId } = person;

  const [drinks, drinksLoading, handleDrink] = useDrinks(personId);

  return (
    <BeerCounter
      drinks={drinks}
      loading={drinksLoading}
      handleDrink={handleDrink}
    />
  );
}
