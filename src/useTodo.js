import { useState, useEffect } from 'react';
import todoService from './todo.service';

export default function useTodo() {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);

  async function loadItems() {
    setLoading(true);
    const loadedItems = await todoService.getItems();
    setItems(loadedItems);
    setLoading(false);
  }

  useEffect(() => {
    loadItems();
  }, []);

  return [items, loading];
}
