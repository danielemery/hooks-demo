import React from "react";
import uuidv4 from "uuid/v4";

import ClassicBeerCounter from "./ClassicBeerCounter";
import AddPersonModal from "../common/AddPersonModal";
import PeopleMenu from "../common/PeopleMenu";

import constants from "../common/constants";

class Classic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      people: constants.samplePeople,
      activePerson: constants.samplePeople[0],
      adding: false,
      addName: ""
    };

    this.handleAddPerson = this.handleAddPerson.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handlePersonChange = this.handlePersonChange.bind(this);
  }

  async handlePersonChange(person) {
    this.setState({ activePerson: person });
  }

  handleAddPerson() {
    const { addName } = this.state;
    const newPerson = {
      name: addName,
      id: uuidv4()
    };
    this.setState(prevState => ({
      people: [...prevState.people, newPerson],
      adding: false,
      addName: ""
    }));
  }

  handleCloseModal() {
    this.setState({ adding: false, addName: "" });
  }

  render() {
    const { people, activePerson, adding, addName } = this.state;
    return (
      <div>
        <PeopleMenu
          people={people}
          activePerson={activePerson}
          handlePersonChange={this.handlePersonChange}
          handleAdd={() => this.setState({ adding: true })}
        />
        <ClassicBeerCounter person={activePerson} />
        <AddPersonModal
          open={adding}
          value={addName}
          onChange={name =>
            this.setState({
              addName: name
            })
          }
          onClose={this.handleCloseModal}
          onSubmit={this.handleAddPerson}
        />
      </div>
    );
  }
}

export default Classic;
