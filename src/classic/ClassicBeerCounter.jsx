import React from "react";

import BeerCounter from "../common/BeerCounter";
import beerService from "../services/beer.service";

class ClassicBeerCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      drinks: [],
      loading: true,
      cancelToken: undefined
    };

    this.handleBeerAdded = this.handleBeerAdded.bind(this);
  }

  async reload() {
    const { person } = this.props;
    this.setState({
      loading: true
    });
    const drinks = await beerService.getBeerCount(person.id);
    this.setState({
      drinks,
      loading: false
    });
  }

  handleBeerAdded(beer) {
    this.setState(prevState => {
      const present = prevState.drinks.find(drink => drink.id === beer.id);
      if (present) {
        return {};
      }
      return {
        drinks: [...prevState.drinks, beer]
      };
    });
  }

  async componentDidMount() {
    const { person } = this.props;
    this.reload();
    this.setState({
      cancelToken: beerService.listenToBeers(person.id, this.handleBeerAdded)
    });
  }

  async componentDidUpdate(prevProps) {
    const { person } = this.props;
    if (prevProps.person.id !== person.id) {
      this.reload();
      this.state.cancelToken();
      this.setState({
        cancelToken: beerService.listenToBeers(person.id, this.handleBeerAdded)
      });
    }
  }

  componentWillUnmount() {
    this.state.cancelToken();
  }

  async handleDrink(personId) {
    const beer = beerService.createBeer();
    this.setState(
      prevState => ({
        drinks: [
          ...prevState.drinks,
          {
            ...beer,
            loading: true
          }
        ]
      }),
      async () => {
        await beerService.drinkBeer(personId, beer);
        this.setState(prevState => ({
          drinks: prevState.drinks.map(drink => {
            if (drink.id === beer.id) {
              return {
                ...drink,
                loading: false
              };
            }
            return drink;
          })
        }));
      }
    );
  }

  render() {
    const { drinks, loading } = this.state;
    const { person } = this.props;
    const { id: personId } = person;
    return (
      <BeerCounter
        drinks={drinks}
        loading={loading}
        handleDrink={() => this.handleDrink(personId)}
      />
    );
  }
}

export default ClassicBeerCounter;
