import uuidv4 from 'uuid/v4';

let lists = {
  1: [
    { name: 'Nutritional Yeast', id: uuidv4() }
  ],
  2: [
    { name: 'Mow the lawn', id: uuidv4() }
  ]
};

export default {
  getItems: async (listId) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(lists[listId]);
      }, 1000);
    });
  },
};