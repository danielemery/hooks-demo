import React from "react";
import { Menu } from "semantic-ui-react";

export default function PeopleMenu(props) {
  const { people, activePerson, handlePersonChange, handleAdd } = props;
  const { id: activePersonId } = activePerson;
  return (
    <Menu>
      {people.map(person => {
        const { id, name } = person;
        return (
          <Menu.Item
            active={activePersonId === person.id}
            key={id}
            onClick={() => handlePersonChange(person)}
          >
            {name}
          </Menu.Item>
        );
      })}
      <Menu.Item onClick={handleAdd} position="right">
        Add
      </Menu.Item>
    </Menu>
  );
}
