import uuidv4 from "uuid/v4";

export default {
  samplePeople: [
    {
      name: "Daniel",
      id: uuidv4()
    },
    {
      name: "Bryan",
      id: uuidv4()
    }
  ]
};
