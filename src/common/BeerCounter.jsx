import React from "react";
import {
  Header,
  Icon,
  Loader,
  Button,
  Dimmer,
  Segment
} from "semantic-ui-react";

export default function BeerCounter(props) {
  const { drinks, handleDrink, loading } = props;
  return (
    <div>
      <Dimmer.Dimmable as={Segment} dimmed={loading} className="beerPanel">
        <Loader />
        {!loading && (
          <div>
            <Button onClick={handleDrink}>Drink!</Button> <br />
            {drinks.map(drink => (
              <Icon
                key={drink.id}
                name="bar"
                disabled={drink.loading}
                size="huge"
              />
            ))}
          </div>
        )}
        <Dimmer active={loading}>
          <Header as="h2" icon inverted>
            <Icon loading={true} name="bar" />
            Loading Your Beers
          </Header>
        </Dimmer>
      </Dimmer.Dimmable>
    </div>
  );
}
