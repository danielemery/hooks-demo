import React from "react";
import { Modal, Input, Button } from "semantic-ui-react";

export default function AddPersonModal(props) {
  const { open, value, onChange, onClose, onSubmit } = props;
  return (
    <Modal open={open} onClose={onClose} size="mini">
      <Modal.Header>Add Person</Modal.Header>
      <Modal.Content>
        <Input
          label="Name"
          value={value}
          onChange={e => onChange(e.target.value)}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={onSubmit} positive>
          Add
        </Button>
        <Button onClick={onClose} negative>
          Cancel
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
