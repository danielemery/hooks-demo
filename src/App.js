import React from "react";
import { Container, Header } from "semantic-ui-react";
import "./App.css";
import ClassicBeer from "./classic/Classic";
import HooksBeer from "./hooks/Hooks";

import "./BeerCounter.scss";

const lists = [
  { name: "Daniel", id: 1 },
  { name: "Bryan", id: 2 },
  { name: "Simon", id: 3 },
  { name: "Ryan", id: 4 }
];

function App() {
  return (
    <Container className="App">
      <Header>Beer Counter 1.0</Header>
      <Header size="small">Classic</Header>
      <ClassicBeer lists={lists} />
      <Header size="small">Hooks</Header>
      <HooksBeer lists={lists} />
    </Container>
  );
}

export default App;
