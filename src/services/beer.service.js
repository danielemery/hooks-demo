import uuidv4 from "uuid/v4";
const beerDrinksByPerson = {};

let listeners = [];

export default {
  createBeer: function() {
    return {
      id: uuidv4(),
      drinkTime: new Date()
    };
  },
  getBeerCount: async function(personId) {
    return new Promise(res => {
      setTimeout(() => res(beerDrinksByPerson[personId] || []), 1000);
    });
  },
  drinkBeer: async function(personId, beer) {
    return new Promise(res => {
      setTimeout(() => {
        // Add the beer
        const currentDrinks = beerDrinksByPerson[personId] || [];
        beerDrinksByPerson[personId] = [...currentDrinks, beer];
        res(beer);

        // Notify listeners
        const interested = listeners.filter(
          listener => listener.personId === personId
        );
        interested.forEach(listener => listener.onBeerAdded(beer));
      }, Math.random() * 5000);
    });
  },
  listenToBeers: function(personId, onBeerAdded) {
    const listenerId = uuidv4();
    listeners = [
      ...listeners,
      {
        id: listenerId,
        personId,
        onBeerAdded
      }
    ];

    function cancel() {
      listeners = listeners.filter(listener => listener.id !== listenerId);
    }
    return cancel;
  }
};
